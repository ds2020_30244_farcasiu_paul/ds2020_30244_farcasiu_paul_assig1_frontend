import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


function getLoginInfo(params, callback) {
    let request = new Request(HOST.backend_api + '/login/' + params.username + '/' + params.password, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function postUser(user, callback){
    let request = new Request(HOST.backend_api + '/login' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getLoginInfo,
    postUser
};
