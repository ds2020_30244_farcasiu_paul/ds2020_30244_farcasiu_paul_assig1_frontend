import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PatientForm from "./components/patient-form";
import * as API_PATIENTS from "./api/patient-api";
import * as API_MEDICATIONPLAN from "../medicationplan/api/medicationplan-api";
import PatientTable from "./components/patient-table";
import MedicationPlanTable from "../medicationplan/components/medicationplan-table";



class PatientContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            tableData4: [],
            isLoaded4: false,
            errorStatus: 0,
            error: null
        };

    }

    componentDidMount() {
        this.fetchPersons();
    }

    fetchPersons() {
        return API_MEDICATIONPLAN.getMedicationPlansWithUUID(window.localStorage.getItem("idPersoana"),(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    tableData4: result,
                    isLoaded4: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPersons();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Patient Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                        <p>
                            {'nume: '+window.localStorage.getItem("namePersoana")}
                        </p>
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                            <p>
                                {'type: '+window.localStorage.getItem("userType")}
                            </p>
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                            <p>
                                {'username: '+window.localStorage.getItem("userUser")}
                            </p>
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                        <p>
                            {'pass: '+window.localStorage.getItem("passUser")}
                        </p>
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded4 && <MedicationPlanTable tableData4 = {this.state.tableData4}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Patient: </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default PatientContainer;
