import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patientCRUD'
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + '/patient/', {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientsWithCaregiver(params, callback) {
    let request = new Request(HOST.backend_api + '/patient/' + params.toString(),{
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}



function deletePatient(params,callback) {
    let request = new Request(HOST.backend_api + '/patient/' + params.name,{
        method: 'DELETE',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(patient, callback){
    let request = new Request(HOST.backend_api + '/patient' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updatePatient(patient, callback){
    let request = new Request(HOST.backend_api + '/patient/update/' + patient.address + '/' + patient.name, {
        method: 'PUT',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getPatients,
    postPatient,
    deletePatient,
    updatePatient,
    getPatientsWithCaregiver
};
