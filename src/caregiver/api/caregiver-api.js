import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiver: '/caregiverCRUD'
};

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + '/caregiver/', {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(params,callback) {
    let request = new Request(HOST.backend_api + '/caregiver/' + params.name,{
        method: 'DELETE',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postCaregiver(patient, callback){
    let request = new Request(HOST.backend_api + '/caregiver' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateCaregiver(patient, callback){
    let request = new Request(HOST.backend_api + '/caregiver/update/' + patient.address + '/' + patient.name, {
        method: 'PUT',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getCaregiverByName(params, callback) {
    let request = new Request(HOST.backend_api + '/caregiver/get/' + params.toString(), {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getCaregivers,
    getCaregiverByName,
    updateCaregiver,
    deleteCaregiver,
    postCaregiver
};