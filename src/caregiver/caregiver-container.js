import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import CaregiverForm from "./components/caregiver-form";
import * as API_CAREGIVERS from "./api/caregiver-api"
import * as API_PATIENTS from "../patient/api/patient-api";
import * as API_MEDICATIONPLAN from "../medicationplan/api/medicationplan-api";
import PatientTable from "../patient/components/patient-table";
import MedicationPlanTable from "../medicationplan/components/medicationplan-table";



class CaregiverContainer extends React.Component {


    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            tableData1: [],
            tableData4: [],
            isLoaded: false,
            isLoaded4: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchPatients();
        this.fetchMedicationPlan();
    }


    fetchPatients() {
        return API_PATIENTS.getPatientsWithCaregiver(window.localStorage.getItem("namePersoana"),(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    tableData1: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchMedicationPlan() {
        return API_MEDICATIONPLAN.getMedicationPlansWithCaregiver(window.localStorage.getItem("namePersoana"),(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    tableData4: result,
                    isLoaded4: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false,
            isLoaded4: false
        });
        this.toggleForm();
        this.fetchPatients();
        this.fetchMedicationPlan()
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Caregivers Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientTable tableData1 = {this.state.tableData1}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded4 && <MedicationPlanTable tableData4 = {this.state.tableData4}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>


            </div>
        )

    }
}


export default CaregiverContainer;