import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patientCRUD'
};

function getMedicationPlans(callback) {
    let request = new Request(HOST.backend_api + '/medicationPlan/', {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPlansWithCaregiver(params, callback) {
    let request = new Request(HOST.backend_api + '/patient/find/' + params.toString(),{
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPlansWithUUID(params, callback) {
    let request = new Request(HOST.backend_api + '/medicationPlan/' + params.toString(),{
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deleteMedicationPlan(params,callback) {
    let request = new Request(HOST.backend_api + '/patient/' + params.name,{
        method: 'DELETE',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedicationPlan(patient, callback){
    let request = new Request(HOST.backend_api + '/medicationPlan/' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateMedicationPlan(patient, callback){
    let request = new Request(HOST.backend_api + '/patient/update/' + patient.address + '/' + patient.name, {
        method: 'PUT',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedicationPlans,
    postMedicationPlan,
    deleteMedicationPlan,
    updateMedicationPlan,
    getMedicationPlansWithCaregiver,
    getMedicationPlansWithUUID
};
