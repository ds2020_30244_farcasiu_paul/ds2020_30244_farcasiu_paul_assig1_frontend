import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Patient id',
        accessor: 'patientid',
    },
    {
        Header: 'Medication',
        accessor: 'medication',
    },
    {
        Header: 'Interval',
        accessor: 'interval',
    },
    {
        Header: 'Date Start',
        accessor: 'dateStart',
    },
    {
        Header: 'Date End',
        accessor: 'dateEnd',
    }
];

const filters = [
    {
        accessor: 'medication',
    }
];

class MedicationPlanTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData4: this.props.tableData4
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData4}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}

export default MedicationPlanTable;