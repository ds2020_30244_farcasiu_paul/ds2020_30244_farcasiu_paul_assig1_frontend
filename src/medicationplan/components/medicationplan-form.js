import React from 'react';
import validateMedicationPlan from "./validators/medicationplan-validators";
import Button from "react-bootstrap/Button";
import * as API_PATIENTS from "../api/medicationplan-api";
import * as API_LOGIN from "../../login/api/login-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class MedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                patientid: {
                    value: '',
                    placeholder: 'patientid',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                interval: {
                    value: '',
                    placeholder: 'interval',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                medication: {
                    value: '',
                    placeholder: 'medication',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                dateStart: {
                    value: '',
                    placeholder: '2016-03-16T13:56:39.492',
                    valid: false,
                    touched: false,
                    validationRules: {
                        dateValidator: true,
                        isRequired: true
                    }
                },
                dateEnd: {
                    value: '',
                    placeholder: '2016-03-16T13:56:39.492',
                    valid: false,
                    touched: false,
                    validationRules: {
                        genderValidator: true,
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validateMedicationPlan(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerMedicationPlan(medicationplan) {
        return API_PATIENTS.postMedicationPlan(medicationplan, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medicationplan with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let medicationplan = {
            patientid: this.state.formControls.patientid.value,
            interval: this.state.formControls.interval.value,
            medication: this.state.formControls.medication.value,
            dateStart: this.state.formControls.dateStart.value,
            dateEnd: this.state.formControls.dateEnd.value,
        };

        console.log(medicationplan);
        this.registerMedicationPlan(medicationplan);
    }

    render() {
        return (
            <div>

                <FormGroup id='patientid'>
                    <Label for='patientidField'> Patientid: </Label>
                    <Input name='patientid' id='patientidField' placeholder={this.state.formControls.patientid.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.patientid.value}
                           touched={this.state.formControls.patientid.touched? 1 : 0}
                           valid={this.state.formControls.patientid.valid}
                           required
                    />
                    {this.state.formControls.patientid.touched && !this.state.formControls.patientid.valid &&
                    <div className={"error-message row"}> * Patient id must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='medication'>
                    <Label for='medicationField'> Medication: </Label>
                    <Input name='medication' id='medicationField' placeholder={this.state.formControls.medication.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medication.value}
                           touched={this.state.formControls.medication.touched? 1 : 0}
                           valid={this.state.formControls.medication.valid}
                           required
                    />
                    {this.state.formControls.medication.touched && !this.state.formControls.medication.valid &&
                    <div className={"error-message row"}> * Medication must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='interval'>
                    <Label for='intervalField'> Interval: </Label>
                    <Input name='interval' id='intervalField' placeholder={this.state.formControls.interval.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.interval.value}
                           touched={this.state.formControls.interval.touched? 1 : 0}
                           valid={this.state.formControls.interval.valid}
                           required
                    />
                    {this.state.formControls.interval.touched && !this.state.formControls.interval.valid &&
                    <div className={"error-message row"}> * Patient id must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='dateStart'>
                    <Label for='dateStartField'> Date Start: </Label>
                    <Input name='dateStart' id='dateStartField' placeholder={this.state.formControls.dateStart.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.dateStart.value}
                           touched={this.state.formControls.dateStart.touched? 1 : 0}
                           valid={this.state.formControls.dateStart.valid}
                           required
                    />
                    {this.state.formControls.dateStart.touched && !this.state.formControls.dateStart.valid &&
                    <div className={"error-message row"}> * Date Start must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='dateEnd'>
                    <Label for='dateEndField'> Date End: </Label>
                    <Input name='dateEnd' id='dateEndField' placeholder={this.state.formControls.dateEnd.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.dateEnd.value}
                           touched={this.state.formControls.dateEnd.touched? 1 : 0}
                           valid={this.state.formControls.dateEnd.valid}
                           required
                    />
                    {this.state.formControls.dateEnd.touched && !this.state.formControls.dateEnd.valid &&
                    <div className={"error-message row"}> * Date End id must have at least 3 characters </div>}
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default MedicationPlanForm;
