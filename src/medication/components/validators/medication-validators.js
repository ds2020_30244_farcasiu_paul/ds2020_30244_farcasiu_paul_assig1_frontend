
const minLengthValidator = (value, minLength) => {
    return value.length >= minLength;
};

const requiredValidator = value => {
    return value.trim() !== '';
};

const moreValidator = (value, minLength) => {
    return value >= minLength ;
};

const lessValidator = (value, maxLength) => {
    return value <= maxLength ;
};




const validateMedication = (value, rules) => {
    let isValid = true;

    for (let rule in rules) {

        switch (rule) {
            case 'minLength': isValid = isValid && minLengthValidator(value, rules[rule]);
                break;

            case 'isRequired': isValid = isValid && requiredValidator(value);
                break;

            case 'isBigger': isValid = isValid && moreValidator(value, rules[rule]);
                break;

            case 'isSmaller': isValid = isValid && lessValidator(value, rules[rule]);
                break;

            default: isValid = true;
        }

    }

    return isValid;
};

export default validateMedication;
