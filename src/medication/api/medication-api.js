import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    medication: '/medicationCRUD'
};

function getMedications(callback) {
    let request = new Request(HOST.backend_api + '/medication/', {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deleteMedication(params,callback) {
    let request = new Request(HOST.backend_api + '/medication/' + params.name,{
        method: 'DELETE',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedication(user, callback){
    let request = new Request(HOST.backend_api + '/medication' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateMedication(medication, callback){
    let request = new Request(HOST.backend_api + '/medication/update/' + medication.effects + '/' + medication.name, {
        method: 'PUT',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedications,
    deleteMedication,
    updateMedication,
    postMedication
};