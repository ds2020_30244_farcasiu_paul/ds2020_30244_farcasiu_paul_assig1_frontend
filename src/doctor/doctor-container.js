import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PatientForm from "../patient/components/patient-form";
import * as API_PATIENTS from "../patient/api/patient-api"
import * as API_CAREGIVERS from "../caregiver/api/caregiver-api"
import * as API_MEDICATIONS from "../medication/api/medication-api"
import * as API_MEDICATIONPLAN from "../medicationplan/api/medicationplan-api"
import PatientTable from "../patient/components/patient-table";
import MedicationTable from "../medication/components/medication-table";
import CaregiverTable from "../caregiver/components/caregiver-table";
import CaregiverForm from "../caregiver/components/caregiver-form";
import MedicationForm from "../medication/components/medication-form";
import DeleteMedicationForm from "../medication/components/delete-medication-form";
import UpdateMedicationForm from "../medication/components/update-medication-form";
import DeleteCaregiverForm from "../caregiver/components/delete-caregiver-form";
import UpdateCaregiverForm from "../caregiver/components/update-caregiver-form";
import DeletePatientForm from "../patient/components/delete-patient-form";
import UpdatePatientForm from "../patient/components/update-patient-form";
import MedicationPlanForm from "../medicationplan/components/medicationplan-form";
import MedicationPlanTable from "../medicationplan/components/medicationplan-table";



class DoctorContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm1 = this.toggleForm1.bind(this);
        this.toggleForm2 = this.toggleForm2.bind(this);
        this.toggleForm3 = this.toggleForm3.bind(this);
        this.toggleForm4 = this.toggleForm4.bind(this);
        this.toggleForm5 = this.toggleForm5.bind(this);
        this.toggleForm6 = this.toggleForm6.bind(this);
        this.toggleForm7 = this.toggleForm7.bind(this);
        this.toggleForm8 = this.toggleForm8.bind(this);
        this.toggleForm9 = this.toggleForm9.bind(this);
        this.toggleForm10 = this.toggleForm10.bind(this);
        this.reload1 = this.reload1.bind(this);
        this.reload2 = this.reload2.bind(this);
        this.reload3 = this.reload3.bind(this);
        this.reload4 = this.reload4.bind(this);
        this.reload5 = this.reload5.bind(this);
        this.reload6 = this.reload6.bind(this);
        this.reload7 = this.reload7.bind(this);
        this.reload8 = this.reload8.bind(this);
        this.reload9 = this.reload9.bind(this);
        this.reload11 = this.reload11.bind(this);
        this.state = {
            selected1: false,
            selected2: false,
            selected3: false,
            selected4: false,
            selected5: false,
            selected6: false,
            selected7: false,
            selected8: false,
            selected9: false,
            selected10: false,
            collapseForm: false,
            tableData1: [],
            tableData2: [],
            tableData3: [],
            tableData4: [],
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
            errorStatus: 0,
            error: null
        };

    }

    componentDidMount() {
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }

    fetchPatients() {
        return API_PATIENTS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData1: result,
                    isLoaded1: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchMedicationPlans() {
        return API_MEDICATIONPLAN.getMedicationPlans((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData4: result,
                    isLoaded4: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchCaregivers() {
        return API_CAREGIVERS.getCaregivers((result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    tableData2: result,
                    isLoaded2: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchMedications() {
        return API_MEDICATIONS.getMedications((result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    tableData3: result,
                    isLoaded3: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm1() {
        this.setState({selected1: !this.state.selected1});
    }
    toggleForm2() {
        this.setState({selected2: !this.state.selected2});
    }
    toggleForm3() {
        this.setState({selected3: !this.state.selected3});
    }
    toggleForm4() {
        this.setState({selected4: !this.state.selected4});
    }
    toggleForm5() {
        this.setState({selected5: !this.state.selected5});
    }
    toggleForm6() {
        this.setState({selected6: !this.state.selected6});
    }
    toggleForm7() {
        this.setState({selected7: !this.state.selected7});
    }
    toggleForm8() {
        this.setState({selected8: !this.state.selected8});
    }
    toggleForm9() {
        this.setState({selected9: !this.state.selected9});
    }
    toggleForm10() {
        this.setState({selected10: !this.state.selected10});
    }



    reload1() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
        });
        this.toggleForm1();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }
    reload2() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
        });
        this.toggleForm2();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }
    reload3() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
        });
        this.toggleForm3();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }
    reload4() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
        });
        this.toggleForm4();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }
    reload5() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
        });
        this.toggleForm5();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }
    reload6() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
        });
        this.toggleForm6();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }
    reload7() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
        });
        this.toggleForm7();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }
    reload8() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
        });
        this.toggleForm8();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }
    reload9() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
        });
        this.toggleForm9();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }
    reload11() {
        this.setState({
            isLoaded1: false,
            isLoaded2: false,
            isLoaded3: false,
            isLoaded4: false,
        });
        this.toggleForm10();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
        this.fetchMedicationPlans();
    }


    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Patient Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm1}>Add Patient </Button>
                            <Button color="primary" onClick={this.toggleForm2}>Update Patient </Button>
                            <Button color="primary" onClick={this.toggleForm3}>Delete Patient </Button>
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm4}>Add Caregiver </Button>
                            <Button color="primary" onClick={this.toggleForm5}>Update Caregiver </Button>
                            <Button color="primary" onClick={this.toggleForm6}>Delete Caregiver </Button>
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm7}>Add Medication </Button>
                            <Button color="primary" onClick={this.toggleForm8}>Update Medication </Button>
                            <Button color="primary" onClick={this.toggleForm9}>Delete Medication </Button>
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm10}>Add Medication Plan</Button>
                            </Col>
                    </Row>
                    <br/>
                    <Row>

                        <Col sm={{size: '8', offset: 1, marginTop:10}}>
                            {this.state.isLoaded1 && <PatientTable tableData1 = {this.state.tableData1}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                        <Col sm={{size: '8', offset: 1, marginTop:10}}>
                            {this.state.isLoaded2 && <CaregiverTable tableData2 = {this.state.tableData2}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>

                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded3 && <MedicationTable tableData3 = {this.state.tableData3}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>

                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded4 && <MedicationPlanTable tableData4 = {this.state.tableData4}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected1} toggle={this.toggleForm1}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm1}> Add Patient: </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reload1}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected2} toggle={this.toggleForm2}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm2}> Update Patient: </ModalHeader>
                    <ModalBody>
                        <UpdatePatientForm reloadHandler={this.reload2}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected3} toggle={this.toggleForm3}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm3}> Delete Patient: </ModalHeader>
                    <ModalBody>
                        <DeletePatientForm reloadHandler={this.reload3}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected4} toggle={this.toggleForm4}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm4}> Add Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverForm reloadHandler={this.reload4}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected5} toggle={this.toggleForm5}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm5}> Update Caregiver: </ModalHeader>
                    <ModalBody>
                        <UpdateCaregiverForm reloadHandler={this.reload5}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected6} toggle={this.toggleForm6}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm6}> Delete Caregiver: </ModalHeader>
                    <ModalBody>
                        <DeleteCaregiverForm reloadHandler={this.reload6}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected7} toggle={this.toggleForm7}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm7}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationForm reloadHandler={this.reload7}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected8} toggle={this.toggleForm8}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm8}> Update Medication: </ModalHeader>
                    <ModalBody>
                        <UpdateMedicationForm reloadHandler={this.reload8}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected9} toggle={this.toggleForm9}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm9}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <DeleteMedicationForm reloadHandler={this.reload9}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected10} toggle={this.toggleForm10}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm10}> Add Medication Plan: </ModalHeader>
                    <ModalBody>
                        <MedicationPlanForm reloadHandler={this.reload11}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default DoctorContainer;
